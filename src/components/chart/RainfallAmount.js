import React, { Component } from 'react';
import Highcharts from 'highcharts';
import axios from 'axios';
import './Chart.css';

const RAINFALL_URL = 'http://private-4945e-weather34.apiary-proxy.com/weather34/rain'

class RainfallAmount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }
    async componentDidMount() {
        try {
            let { data=[] } = await axios.get(RAINFALL_URL);
            console.log(data)
            
        } catch (err) {

        }

    }


    render() {
        return (
            <div id="Rainfall"></div>
        );
    }
}

export default RainfallAmount;