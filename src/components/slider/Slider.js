import React, { Component } from 'react';
import './Slider.css';

class Slider extends Component {
    constructor(props){
        super(props);
        this.state={
            value: this.props.min
        }
    }
    onChange =(e) =>{
        this.setState({
            value: e.target.value
        })
    }
    render() {
        return (
        <div className="Slider">
            <input value={this.state.value} className="Slider__input" type="range" min={this.props.min} max={this.props.max} onChange={this.onChange} />
            <div className="Slider__value">
                {this.state.value}
            </div>
        </div>
        );
    }
}

export default Slider;