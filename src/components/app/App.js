import React, { Component } from 'react';
import Widget from './Widget';
import Slider from '../slider/Slider';
import RainfallAmount from '../chart/RainfallAmount';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App__modules">
          <div className="App__module">
            <Widget title="Pressure">
              <Slider min="970" max="1030" />
            </Widget>
            <Widget title="Temperature">
              <Slider min="10" max="35" />
            </Widget>
          </div>
          <div className="App__module">
            <Widget title="Chances of rainfall">

            </Widget>
            <Widget title="Amount of rainfall">
              <RainfallAmount />
            </Widget>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
