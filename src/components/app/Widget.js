import React, { Component } from 'react';
import './Widget.css';

class Widget extends Component {
  render() {
    return (
      <div className="Widget">
          <div className="Widget__title">
            {this.props.title}
          </div>
          <div className="Widget__content">
            {this.props.children}
          </div>
      </div>
    );
  }
}

export default Widget;
